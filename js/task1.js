/* Теоритичні питання:
1. В чому полягає відмінність localStorage і sessionStorage?
2. Які аспекти безпеки слід враховувати при збереженні чутливої інформації, такої як паролі, за допомогою localStorage чи sessionStorage?
3. Що відбувається з даними, збереженими в sessionStorage, коли завершується сеанс браузера?

Практичне завдання:

Реалізувати можливість зміни колірної теми користувача.

Технічні вимоги:

- Взяти готове домашнє завдання HW-4 "Price cards" з блоку Basic HMTL/CSS.
- Додати на макеті кнопку "Змінити тему".
- При натисканні на кнопку - змінювати колірну гаму сайту (кольори кнопок, фону тощо) на ваш розсуд. 
    При повторному натискання - повертати все як було спочатку - начебто для сторінки доступні дві колірні теми.
- Вибрана тема повинна зберігатися після перезавантаження сторінки.

Примітки: 
- при виконанні завдання перебирати та задавати стилі всім елементам за допомогою js буде вважатись помилкою;
- зміна інших стилів сторінки, окрім кольорів буде вважатись помилкою.

Додаткові матеріали: https://developer.mozilla.org/en-US/docs/Web/CSS/Using_CSS_custom_properties.
*/ 


console.log("THEORY");

console.log("1. localstorage переживе перезавантаження ОС або браузера, закриття вкладки, внесення змін в його память валідне для кожної вкладки цього браузера.ю воно загальне. sessionStorage обмежене тільки перезавантаженням сторінки, кожна сторінка має свій sessionStorage, вони не взаємодіють + sessionStorage - набагато менш практичне ");
console.log("2. До цієї інформації може отримати доступ будь-яка людина, вона не захищена. Це радше для збереження, важливої , але не приватної інформації");
console.log("3. Видаляється і не зберігається");



console.log("PRACTICE");

const darkmodeButton = document.querySelector(".darkmode"); //кнопка для зміни кольору
// console.log(darkmodeButton);
const themeChanger = document.querySelector("#css"); //link отримує змінну за айді

darkmodeButton.addEventListener("click", () => {
    //document.body.classList.toggle("dark-theme");
    // document.getElementById("css").setAttribute("href", "./CSS/darkmode.css").setAttribute("data-changedTheme", "yes");
     
    if (themeChanger.getAttribute("href") === "./CSS/styles.css") { //якщо дорівнює звичайному стилю. то включаємо темний
        themeChanger.href = "./CSS/darkmode.css";
        localStorage.setItem("theme",  "./CSS/darkmode.css");
    } else { //якщо дорівнює темному, то включаємо світлий
        themeChanger.href = "./CSS/styles.css";
        localStorage.setItem("theme",  "./CSS/styles.css");
    }
    
});

document.addEventListener("DOMContentLoaded", () => { //функція для отримання значення зі сховища про тему цього сайту, підгрузить темну або світлу, відповідно до того, яку юзер вибрав раніше
    function getTheme () {
        themeChanger.href = localStorage.getItem("theme") || "./CSS/styles.css";
        return themeChanger;
    }
    getTheme();
});


